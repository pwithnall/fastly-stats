fastly-stats
===

This is a simple set of tools to extract statistics from the API provided by
[fastly](https://developer.fastly.com/reference/api/). fastly is a content
delivery network (CDN).

Currently, the aim of the tools is to expose bandwidth data for the purposes
of calculating the carbon footprint of content delivery for a particular
service.

Configuration
---

To use these tools you will need a fastly account connected to the services
you want to query, and will need to have created an API token with
`global:read` permissions on the
[API tokens page](https://manage.fastly.com/account/personal/tokens).

You will need to pass the token to the tools using the `--token` command line
operation, or put it in a `./credentials.ini` file containing:
```
[api.fastly.com]
token = my-access-token
```

Contributing
---

Contributions are welcome as merge requests.

License
---

These tools are licensed as GPLv3+.
