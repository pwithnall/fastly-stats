#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2021 Endless OS Foundation, LLC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import argparse
import configparser
import csv
import datetime
import requests


# Query the number of requests and bandwidth consumption for all accessible
# services in time period given by --start-date and --end-date. Dump the
# per-(service, day) data to fastly-stats.csv, and print totals as a summary
# to stdout.
#
# You can configure your token by creating a `./credentials.ini` file
# containing:
#
# ```
# [api.fastly.com]
# token = my-access-token
# ```


# Derived from https://stackoverflow.com/a/49361727/2931197
def format_bytes(size):
    power = 10 ** 3
    n = 0
    power_labels = {0: "B", 1: "KB", 2: "MB", 3: "GB", 4: "TB"}
    while size > power:
        size /= power
        n += 1
    return format(size, ",.2f") + power_labels[n]


def request(path, params, token):
    response = requests.get(
        "https://api.fastly.com" + path, params=params, headers={"Fastly-Key": token}
    )
    response.raise_for_status()
    response_json = response.json()
    if response_json["status"] != "success":
        raise Exception("Request failed (status: {})".format(response_json["status"]))

    return response_json["data"]


def main():
    end_date = datetime.date.today()
    start_date = end_date - datetime.timedelta(days=30)
    token = None

    # Load the token. This should be generated on
    # https://manage.fastly.com/account/personal/tokens and have `global:read`
    # permission.
    try:
        config = configparser.ConfigParser()
        config.read("credentials.ini")
        token = config["api.fastly.com"]["token"]
    except Exception:
        pass

    parser = argparse.ArgumentParser(description="Dump fastly statistics")
    parser.add_argument(
        "--token",
        "-t",
        type=str,
        default=token,
        required=(token is None),
        help="API token (default: loaded from credentials.ini)",
    )
    parser.add_argument(
        "-s",
        "--start-date",
        help="start date for dump, inclusive (format: YYYY-MM-DD, default: "
        "30 days ago)",
        type=datetime.date.fromisoformat,
        default=start_date,
    )
    parser.add_argument(
        "-e",
        "--end-date",
        help="end date for dump, inclusive (format: YYYY-MM-DD, default: today)",
        type=datetime.date.fromisoformat,
        default=end_date,
    )

    args = parser.parse_args()

    # Request the stats. API docs are here:
    # https://developer.fastly.com/reference/api/metrics-stats/historical-stats/
    stats_json = request(
        "/stats",
        params={
            "from": args.start_date.isoformat(),
            "to": args.end_date.isoformat(),
            "by": "day",
            "region": "all",
        },
        token=args.token,
    )

    # Dump them to CSV
    with open("fastly-stats.csv", "x", newline="") as stats_file:
        # Data model is here:
        # https://developer.fastly.com/reference/api/metrics-stats/historical-stats/#results-data-model
        columns = {
            "Service ID": ("service_id", None),
            "Period start": (
                "start_time",
                lambda timestamp: datetime.datetime.fromtimestamp(
                    timestamp
                ).isoformat(),
            ),
            "Number of requests": ("requests", None),
            "Bandwidth (bytes)": ("bandwidth", None),
        }

        writer = csv.writer(stats_file)
        writer.writerow(columns.keys())
        for service_data in stats_json.values():
            for row in service_data:
                writer.writerow(
                    [
                        (formatter if formatter is not None else lambda x: x)(row[col])
                        for (col, formatter) in columns.values()
                    ]
                )

    # Print a summary
    n_requests = sum(
        [
            row["requests"]
            for service_data in stats_json.values()
            for row in service_data
        ]
    )
    bandwidth = sum(
        [
            row["bandwidth"]
            for service_data in stats_json.values()
            for row in service_data
        ]
    )

    print(
        "Period {} to {}:".format(
            args.start_date.isoformat(), args.end_date.isoformat()
        )
    )
    print(f" • Number of requests: {n_requests:,}")
    print(f" • Bandwidth: {format_bytes(bandwidth)}")


if __name__ == "__main__":
    main()
